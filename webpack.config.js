/*
 *     Date: 2023
 *  Package: core-fe-top-scroller
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-fe-top-scroller
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

const path = require('path');

module.exports = {
  mode: 'production',
  entry: './src/core-fe-top-scroller.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'core-fe-top-scroller.js',
    library: "coreFeTopScroller",
    libraryTarget: 'umd',
    globalObject: 'this'
  }
};

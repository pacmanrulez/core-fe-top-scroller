# core-fe-top-scroller

Top scroller.

# License
[WTFPL v2](http://www.wtfpl.net/txt/copying).

# Usage

Simply add the `data-component` to the element anf import the module.

```javascript
import TopScroller from 'core-fe-top-scroller';
TopScroller.bind({
    topOffset: 0,
    visibleFromTop: 600,
    element: '[data-component="top-scroller"]'
});
```

```html
<section class="top-scroller" data-component="top-scroller">
    <a>top</a>
</section>
```

# Author
dbf, pacmanrulez, wize-wiz, wizdom.~~
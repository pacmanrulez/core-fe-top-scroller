/*
 *     Date: 2023
 *  Package: core-fe-top-scroller
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-fe-top-scroller
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

module.exports = (function () {

    if (!document) {
        throw new Error('Should run a in an environment where DOMDocument is available.');
    }

    const
        PACKAGE = {
            name: 'TopScroller'
        },
        WINDOW = window,
        DOCUMENT = document,

        Package = require('core-utilities/utility/package'),
        ElementScroller = require('core-element-scroller'),
        DOM = require('core-utilities/utility/dom')
    ;

    let timeout = null;

    TopScroller = function() {};

    TopScroller.isScrolling = false;

    TopScroller.isVisible = function(offset) {
        offset = offset || 600;

        let docEl = DOCUMENT.documentElement,
            top = (WINDOW.scrollY || docEl.scrollTop) - (docEl.clientTop || 0);

        return top > offset;
    }

    TopScroller.setVisible = function(element, offset, visible) {
        visible = visible || this.isVisible(offset);

        if(element)
            element.classList[visible ? 'add' : 'remove']('active');
    }

    TopScroller.bind = function(options) {

        options = {
            ...{
                topOffset: 0,
                visibleFromTop: 600,
                element: '[data-component="top-scroller"]'
            }, ...options
        }

        let element = DOM.get(options.element);

        if(!element)
            return;

        WINDOW.addEventListener('scroll',() => {
            if(timeout) { clearTimeout(timeout); }
            timeout = setTimeout(() => {
                this.setVisible(element, options.visibleFromTop);
            }, 50);
        });

        element.addEventListener('click', () => {
            // failsafe
            setTimeout(() => {
                this.isScrolling = false;
                // at least after 1 second.
            }, 1000);

            if(this.isScrolling === true)
                return;

            ElementScroller.scrollTop({}, () => {
                this.isScrolling = false;
            });

            this.isScrolling = true;
        });
    }


    PACKAGE.TopScroller = TopScroller;
    // assign to global object core
    return Package.return(PACKAGE);

})();
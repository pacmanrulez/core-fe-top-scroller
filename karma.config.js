/*
 *     Date: 2023
 *  Package: core-fe-top-scroller
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-fe-top-scroller
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

const
    path = require('path'),
    webpackConfig = require('./webpack.config.js');

webpackConfig.entry = {};
webpackConfig.output.path = path.resolve(__dirname, 'spec/dist'),

    module.exports = function(config) {
        config.set({
            basePath: '',
            frameworks: ['jasmine'],
            files: ['spec/**/*.spec.js'],
            exclude: [],
            preprocessors: {
                ['spec/**/*.spec.js'] : ['webpack'],
            },
            webpack: webpackConfig,
            webpackMiddleware: {
                noInfo: true
            },
            reporters: ['progress'],
            port: 9876,
            colors: true,
            logLevel: config.LOG_INFO,
            autoWatch: false,
            browsers: ['Chrome'],
            singleRun: true,
            concurrency: Infinity
        })
    }
